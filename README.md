# Testing Grounds

Based on ["Unreal Engine C++ Developer: Learn C++ and Make Video Games by GameDev.tv" course](https://www.udemy.com/unrealcourse/). Further implemented a different health/death mechanism, a spawned objects cleanup process and features added after version 1.0.0.0.

## Features added after version 1.0.0.0
* Main, pause and death menus.
* Character running.
* Health pickups.